package config

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

// Config defines the parameters for the Logger.
type Config struct {
	OutputLogger         *log.Logger
	Tails                sync.Map
	Poll                 bool
	Exclude              []string
	JSON                 bool
	MinLevel             int
	PidToWatch           int
	TruncateLogs         bool
	TruncateLogsInterval int
	MaxLogFileSize       int64
	WatchCycle           time.Duration
	DebugMode            bool
	ReadFromEnd          bool
	ShowVersion          bool
	CompleteLines        bool
}

func envValueBool(key string, defaultVal bool) bool {
	resVal := defaultVal
	if envVal, ok := os.LookupEnv(key); ok {
		if convVal, err := strconv.ParseBool(envVal); err == nil {
			resVal = convVal
		}
	}
	return resVal
}

func envValueInt(key string, defaultVal int) int {
	resVal := defaultVal
	if envVal, ok := os.LookupEnv(key); ok {
		if convVal, err := strconv.Atoi(envVal); err == nil {
			resVal = convVal
		}
	}
	return resVal
}

// New creates a new Config with defaults from environment variables.
func New(args []string) *Config {
	c := &Config{OutputLogger: log.Default()}

	pollDefault := envValueBool("GITLAB_LOGGER_POLL", false)
	jsonDefault := envValueBool("GITLAB_LOGGER_JSON", false)
	truncateLogsDefault := envValueBool("GITLAB_LOGGER_TRUNCATE_LOGS", false)
	minLevelDefault := envValueInt("GITLAB_LOGGER_MINLEVEL", 0)
	watchPidDefault := envValueInt("GITLAB_LOGGER_WATCH_PID", 0)
	truncateLogsIntervalDefault := envValueInt("GITLAB_LOGGER_TRUNCATE_INTERVAL", 300)
	maxLogFileSizeDefault := envValueInt("GITLAB_LOGGER_MAX_FILESIZE", 1000)
	readFromEndDefault := envValueBool("GITLAB_LOGGER_READ_FROM_END", false)
	completeLinesDefault := envValueBool("GITLAB_LOGGER_COMPLETE_LINES", true)
	c.Exclude = []string{"sasl", "config", "lock", "@", "gzip", "tgz", "gz"}
	debugDefault := envValueBool("GITLAB_LOGGER_DEBUG", false)
	fs := flag.NewFlagSet("gitlab-logger", flag.ExitOnError)

	fs.BoolVar(&c.DebugMode, "debug", debugDefault, "Be extra verbose. All debug information will be printed to stderr")
	fs.BoolVar(&c.ShowVersion, "version", false, "Print version and exit")
	fs.BoolVar(&c.Poll, "poll", pollDefault, "Use polling instead of inotify")
	fs.BoolVar(&c.JSON, "json", jsonDefault, "Keep only JSON logs")
	fs.BoolVar(&c.ReadFromEnd, "read-from-end", readFromEndDefault, "Read from the end of files at startup, rather than all of the file")
	fs.BoolVar(&c.TruncateLogs, "truncate-logs", truncateLogsDefault, "Truncate logs when they reach maximum allowed file size")
	fs.BoolVar(&c.CompleteLines, "complete-lines", completeLinesDefault, "Only emit complete lines")
	fs.IntVar(&c.MinLevel, "minlevel", minLevelDefault, "Minimum log level")
	fs.IntVar(&c.PidToWatch, "watch-pid", watchPidDefault, "The PID of the process to watch")
	fs.IntVar(&c.TruncateLogsInterval, "truncate-logs-interval", truncateLogsIntervalDefault, "Interval in seconds in which to truncate log files")
	fs.Int64Var(&c.MaxLogFileSize, "max-log-file-size", int64(maxLogFileSizeDefault), "Maximum allowed log file size in bytes before file is truncated")
	fs.Func("exclude-keyword", "Exclude any file with full path containing keyword (could be added multiple times)", func(s string) error {
		c.Exclude = append(c.Exclude, s)
		return nil
	})
	// for backward compatibility we'll keep both "exclude" and "exclude-list"
	for _, option := range []string{"exclude-list", "exclude"} {
		var deprecation string
		if option == "exclude" {
			deprecation = "(DEPRECATED)"
		}
		fs.Func(option, deprecation+"Exclude list (use '|' as a separator): excludes any file with full path containing any of keywords provided", func(s string) error {
			c.Exclude = strings.Split(s, "|")
			return nil
		})
	}

	c.WatchCycle = time.Millisecond * 100
	// Below code prevents Usage screen from being displayed:
	// fs.SetOutput(ioutil.Discard)
	fs.Usage = func() { // [4]
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s\n", os.Args[0])
		fs.PrintDefaults()
		fmt.Fprint(flag.CommandLine.Output(), `
	Values for flags can be supplied by environment variables (see README )

	CLI invocation flags overrides environment variable values`)
	}

	// parse and check for errors
	if err := fs.Parse(args); err != nil {
		os.Exit(1)
	}

	return c
}
