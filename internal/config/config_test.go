package config

import (
	"log"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestDefaults(t *testing.T) {
	c := New(nil)

	require.Equal(t, log.Default(), c.OutputLogger)
	require.Equal(t, []string{"sasl", "config", "lock", "@", "gzip", "tgz", "gz"}, c.Exclude)
	require.False(t, c.ShowVersion)
	require.False(t, c.Poll)
	require.False(t, c.JSON)
	require.Equal(t, 0, c.MinLevel)
	require.Equal(t, 0, c.PidToWatch)
	require.False(t, c.TruncateLogs)
	require.Equal(t, 300, c.TruncateLogsInterval)
	require.Equal(t, int64(1000), c.MaxLogFileSize)
	require.Equal(t, time.Millisecond*100, c.WatchCycle)
	require.False(t, c.DebugMode)
	require.False(t, c.ReadFromEnd)
	require.False(t, c.ShowVersion)
	require.True(t, c.CompleteLines)
}
