package logger

import (
	"bytes"
	"encoding/json"
	"gitlab-org/cloud-native/gitlab-logger/internal/config"
	"log"
	"math/rand"
	"os"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

type logInterceptor struct {
	m      sync.Mutex
	output *bytes.Buffer
}

func generateRandomString(length int) string {
	rand.Seed(time.Now().UnixNano())

	// Define the character set from which to generate random characters
	charset := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	// Create a byte slice to store the random characters
	randomBytes := make([]byte, length)

	// Fill the byte slice with random characters from the charset
	for i := 0; i < length; i++ {
		randomBytes[i] = charset[rand.Intn(len(charset))]
	}

	// Convert the byte slice to a string
	return string(randomBytes)
}

func (l *logInterceptor) Write(p []byte) (n int, err error) {
	l.m.Lock()
	defer l.m.Unlock()
	return l.output.Write(p)
}

func setupOutputLogger() (*log.Logger, *logInterceptor) {
	output := &bytes.Buffer{}
	interceptor := &logInterceptor{output: output}
	logger := log.New(interceptor, "", log.LstdFlags)
	log.SetOutput(logger.Writer())
	return logger, interceptor
}

func newConfig(outputLogger *log.Logger) *config.Config {
	config := &config.Config{OutputLogger: outputLogger, WatchCycle: time.Millisecond * 100}

	return config
}

func isValidJSON(line string) bool {
	var data interface{}
	err := json.Unmarshal([]byte(line), &data)
	return err == nil
}

func TestWatchDirectoryAndFiles(t *testing.T) {
	outputLogger, interceptor := setupOutputLogger()
	c := newConfig(outputLogger)
	logger := New(c)

	dir1, err := os.MkdirTemp("", "watch_dir_")
	require.NoError(t, err)
	defer os.RemoveAll(dir1)

	dir2, err := os.MkdirTemp("", "watch_files_")
	require.NoError(t, err)
	defer os.RemoveAll(dir2)

	tmp1, err := os.CreateTemp(dir1, "test1_")
	require.NoError(t, err)
	defer os.Remove(tmp1.Name())

	tmp2, err := os.CreateTemp(dir2, "test2_")
	require.NoError(t, err)
	defer os.Remove(tmp2.Name())

	logger.Start([]string{dir1, tmp2.Name()})
	defer logger.Stop()

	output1 := "a line in a watched dir"
	for i := 0; i < 5; i++ {
		_, err = tmp1.Write([]byte(output1 + "\n"))
		require.NoError(t, err)
		tmp1.Sync()
	}

	output2 := "a line in a watched file"
	for i := 0; i < 5; i++ {
		_, err = tmp2.Write([]byte(output2 + "\n"))
		require.NoError(t, err)
		tmp2.Sync()
	}

	tmp3, err := os.CreateTemp(dir1, "test3_")
	require.NoError(t, err)
	defer os.Remove(tmp3.Name())

	output3 := "a line in a newly-created file"
	for i := 0; i < 5; i++ {
		_, err = tmp3.Write([]byte(output3 + "\n"))
		require.NoError(t, err)
		tmp3.Sync()
	}

	timeout := time.After(10 * time.Second)

	for {
		select {
		case <-timeout:
			t.Error("Timeout reached")
			return
		default:
			output := strings.TrimRight(interceptor.output.String(), "\n")
			lines := strings.Split(output, "\n")

			for _, line := range lines {
				require.True(t, isValidJSON(line))
			}

			if len(lines) == 15 &&
				strings.Contains(output, output1) &&
				strings.Contains(output, output2) &&
				strings.Contains(output, output3) {
				return
			}

			time.Sleep(c.WatchCycle)
		}
	}
}

func TestTruncate(t *testing.T) {
	c := newConfig(log.Default())
	c.TruncateLogs = true
	c.MaxLogFileSize = 2048
	c.TruncateLogsInterval = 1
	logger := New(c)

	dir1, err := os.MkdirTemp("", "watch_dir_")
	require.NoError(t, err)
	defer os.RemoveAll(dir1)

	tmp1, err := os.CreateTemp("", "test1_")
	require.NoError(t, err)
	defer os.Remove(tmp1.Name())

	tmp2, err := os.CreateTemp(dir1, "test2_")
	require.NoError(t, err)
	defer os.Remove(tmp2.Name())

	logger.Start([]string{dir1, tmp1.Name()})
	defer logger.Stop()

	s := generateRandomString(1024)

	for i := 0; i < 5; i++ {
		_, err = tmp1.Write([]byte(s))
		require.NoError(t, err)
		tmp1.Sync()

		_, err = tmp2.Write([]byte(s))
		require.NoError(t, err)
		tmp2.Sync()
	}

	interval := time.Duration(c.TruncateLogsInterval) * time.Second * 2
	time.Sleep(interval)

	f, err := os.Stat(tmp1.Name())
	require.NoError(t, err)
	require.Zero(t, f.Size())

	f, err = os.Stat(tmp2.Name())
	require.NoError(t, err)
	require.Zero(t, f.Size())
}
